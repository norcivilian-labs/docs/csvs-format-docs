# Design

plain-text relational database

competes: recutils, sql

interacts: filesystem, clients, text editors

constitutes: a set of csv files

includes: config, schema, data

resembles: recutils

patterns: 

stakeholders: fetsorn

Specification
- [0.0.2](./0.0.2.md)
- [0.0.1](./0.0.1.md) (deprecated)

Also see the [Requirements](./requirements.md).
