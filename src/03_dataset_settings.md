# Dataset Settings

if you want to store arbitrary settings, try to write them as branches.

you can also write new key-value pairs to the `.csvs.csv` file.

to store credentials and sensitive data, don't write them to the dataset near other data

you can make a separate dataset for sensitive data with stricter security practices

if you use git for version control, you can write sensitive key-value pairs to .git/config

To learn more about csvs, see [Design](./design.md) and [Requirements](./requirements.md).
