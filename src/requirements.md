# Requirements
## shrug drive shed
- user must view dataset in a text editor
## family park lazy
- user must version control the dataset
## pistol puzzle own
- user must query with grep
## adapt satoshi limb
- user must deduplicate values
## render elegant inner
- user without a client should figure out the dataset structure
## bubble immense boat
- developer without a client should figure out how to restore data
## field woman slot
- developer must implement searches
## fine unveil juice
- user must specify relations between entities
## attract chief school
- user must store arrays, lists
## raccoon leave turn
- user must store records, sets
## will face innocent
- user must store escaped strings
## cattle fork depth
- user must store similar data in efficient space
## title shell snap
- user should specify foreign keys as sha256sum hashes of values
## scout path cousin
- user should specify foreign keys as multiformats hashes of values
## inch thunder bind
- user should store relative values
## snack heavy square
- user should store escaped string
## under pass useful
- user should store without duplication
## deputy another health
- user must store values
## bean weapon rely
- developer must extend
## upon digital execute
- user must read plain text
## layer blind glide
- user must read dataset files
## stick congress label
- user must write dataset files
## relax slender wise
- developer must easily reverse engineer
## club step tennis
- developer must must easily support
