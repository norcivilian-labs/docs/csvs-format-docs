# User Guides

 - [Nested Records](./01_nested_records.md)
 - [Lists of Values](./02_lists_of_values.md)
 - [Dataset Settings](./03_dataset_settings.md)
 - [Asset Storage](./04_asset_storage.md)
 - [Branch Metadata](./05_branch_metadata.md)
 - [Writing a Client](./06_writing_a_client.md)

To learn more about csvs, see [Design](./design.md) and [Requirements](./requirements.md).
